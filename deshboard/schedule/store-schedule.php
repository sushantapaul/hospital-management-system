<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])){

        $name = $_POST['name'];
        $day = $_POST['day'];
        $timestart = $_POST['timestart'];
        $timeend = $_POST['timeend'];
        $quantity = $_POST['quantity'];
        $activity = $_POST['activity'];

        if(empty($name)) {
            $_SESSION['msz'] = "Name is required";
            header('Location: ./add-schedule.php');
        }
        elseif(empty($day)) {
            $_SESSION['msz'] = "Day is required";
            header('Location: ./add-schedule.php');
        }
        elseif(empty($timestart)) {
            $_SESSION['msz'] = "Time is required";
            header('Location: ./add-schedule.php');
        }
        elseif(empty($timeend)) {
            $_SESSION['msz'] = "Time is required";
            header('Location: ./add-schedule.php');
        }
        elseif(empty($quantity)) {
            $_SESSION['msz'] = "Patient quantity is required";
            header('Location: ./add-schedule.php');
        }
        elseif(empty($activity)) {
            $_SESSION['msz'] = "Activity is required";
            header('Location: ./add-schedule.php');
        }
        else {            
            $sqldoc = "SELECT * FROM `doctor` WHERE id =".$name;
            $result = $conn->query($sqldoc);

            if($row = $result->fetch_assoc()) {
                $dcname = $row['name'];
    
                $sql = "INSERT INTO `schedule`(`name`, `day`, `timestart`, `timeend`, `quantity`, `activity`) VALUES ('$dcname', '$day', '$timestart', '$timeend', '$quantity', '$activity')";
                
                $_SESSION['success'] = "Data Upload Successfully!";
                header('Location: ./schedule-list.php');

                $conn->query($sql);
            }
            else {
                header('Location: ./schedule-list.php');
            }
        }
    }
    else {
        header('Location: ./schedule-list.php');
    }

?>