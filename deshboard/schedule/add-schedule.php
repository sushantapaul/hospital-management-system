<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php session_start(); ?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Create Doctors Schedule</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="./schedule-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Schedule List</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <form action="./store-schedule.php" method="POST">
                            <div class="form-group row">
                                <label for="drname" class="col-sm-3 col-form-label">Doctor Name</label>
                                <div class="col-sm-6">
                                    <select name="name" class="form-control" id="drname">
                                        <option value="" disabled selected>Select Doctor</option>
                                        <?php
                                            include('../connect.php');
                                            $sqldoc = "SELECT * FROM `doctor`";
                                            $result = $conn->query($sqldoc);

                                            while($row = $result->fetch_assoc()){ ?>
                                                <option style="text-transform: capitalize;" value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="day" class="col-sm-3 col-form-label">Available Day</label>
                                <div class="col-sm-6">
                                    <select name="day" class="form-control" id="day" placeholder="">
                                        <option value="" selected disabled>Select a day</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thuresday">Thuresday</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="availabletime" class="col-sm-3 col-form-label">Available Time</label>
                                <div class="col-sm-3">
                                    <select name="timestart" class="form-control" id="day" placeholder="">
                                        <option value="" selected disabled>Begining Time</option>
                                        <option value="9 am">9 am</option>
                                        <option value="10 am">10 am</option>
                                        <option value="11 am">11 am</option>
                                        <option value="12 pm">12 pm</option>
                                        <option value="1 pm">1 pm</option>
                                        <option value="2 pm">2 pm</option>
                                        <option value="3 pm">3 pm</option>
                                        <option value="4 pm">4 pm</option>
                                        <option value="5 pm">5 pm</option>
                                        <option value="6 pm">6 pm</option>
                                        <option value="7 pm">8 pm</option>
                                        <option value="9 pm">9 pm</option>
                                        <option value="10 pm">10 pm</option>
                                        <option value="11 pm">11 pm</option>
                                        <option value="12 am">12 am</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="timeend" class="form-control" id="day" placeholder="">
                                        <option value="" selected disabled>Ending Time</option>
                                        <option value="9 am">9 am</option>
                                        <option value="10 am">10 am</option>
                                        <option value="11 am">11 am</option>
                                        <option value="12 pm">12 pm</option>
                                        <option value="1 pm">1 pm</option>
                                        <option value="2 pm">2 pm</option>
                                        <option value="3 pm">3 pm</option>
                                        <option value="4 pm">4 pm</option>
                                        <option value="5 pm">5 pm</option>
                                        <option value="6 pm">6 pm</option>
                                        <option value="7 pm">8 pm</option>
                                        <option value="9 pm">9 pm</option>
                                        <option value="10 pm">10 pm</option>
                                        <option value="11 pm">11 pm</option>
                                        <option value="12 am">12 am</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="quantity" class="col-sm-3 col-form-label">Maximum Patient</label>
                                <div class="col-sm-6">
                                    <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Enter Maximum Patient" min="1" max="500">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                <div class="col-sm-6">
                                    <select name="activity" class="form-control" id="" placeholder="" >
                                        <option value="" disabled selected>Select Activity</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="submit" class="btn btn-success form-control">save</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>




