<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $id = $_GET['id'];
    if(empty($id)) {
        header('location: ./schedule-list.php');
    }
    else {    
        $sql2 = "SELECT * FROM `schedule` WHERE id=$id";
        $result2 = $conn->query($sql2);
        $row2 = $result2->fetch_assoc();
        if(empty($row2)) {
            header('location: ./schedule-list.php');
        }
    }
?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Edit Doctors Schedule</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="./schedule-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Schedule List</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <form action="./update-schedule.php" method="POST">
                            <input type="text" name="id" value="<?php echo $row2['id']; ?>" style="display: none;">
                            <div class="form-group row">
                                <label for="drname" class="col-sm-3 col-form-label">Doctor Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="form-control" id="drname" value="<?php echo $row2['name']; ?>" style="color: green;" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="day" class="col-sm-3 col-form-label">Available Day</label>
                                <div class="col-sm-6">
                                    <select name="day" class="form-control" id="day" style="color: red;">
                                        <option value="" selected disabled>Select a day</option>
                                        <option <?php if($row2['day'] == "Friday"){ echo "selected";}?> value="Friday">Friday</option>
                                        <option <?php if($row2['day'] == "Saturday"){ echo "selected";}?> value="Saturday">Saturday</option>
                                        <option <?php if($row2['day'] == "Sunday"){ echo "selected";}?> value="Sunday">Sunday</option>
                                        <option <?php if($row2['day'] == "Monday"){ echo "selected";}?> value="Monday">Monday</option>
                                        <option <?php if($row2['day'] == "Tuesday"){ echo "selected";}?> value="Tuesday">Tuesday</option>
                                        <option <?php if($row2['day'] == "Wednesday"){ echo "selected";}?> value="Wednesday">Wednesday</option>
                                        <option <?php if($row2['day'] == "Thuresday"){ echo "selected";}?> value="Thuresday">Thuresday</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="availabletime" class="col-sm-3 col-form-label">Available Time</label>
                                <div class="col-sm-3">
                                    <select name="timestart" class="form-control" id="day" placeholder="">
                                        <option value="" disabled>Begining time</option>
                                        <option <?php if($row2['timestart'] == "9 am"){ echo "selected";}?> value="9 am">9 am</option>
                                        <option <?php if($row2['timestart'] == "10 am"){ echo "selected";}?> value="10 am">10 am</option>
                                        <option <?php if($row2['timestart'] == "11 am"){ echo "selected";}?> value="11 am">11 am</option>
                                        <option <?php if($row2['timestart'] == "12 pm"){ echo "selected";}?> value="12 pm">12 pm</option>
                                        <option <?php if($row2['timestart'] == "1 pm"){ echo "selected";}?> value="1 pm">1 pm</option>
                                        <option <?php if($row2['timestart'] == "2 pm"){ echo "selected";}?> value="2 pm">2 pm</option>
                                        <option <?php if($row2['timestart'] == "3 pm"){ echo "selected";}?> value="3 pm">3pm</option>
                                        <option <?php if($row2['timestart'] == "4 pm"){ echo "selected";}?> value="4 pm">4 pm</option>
                                        <option <?php if($row2['timestart'] == "5 pm"){ echo "selected";}?> value="5 pm">5 pm</option>
                                        <option <?php if($row2['timestart'] == "6 pm"){ echo "selected";}?> value="6 pm">6 pm</option>
                                        <option <?php if($row2['timestart'] == "7 pm"){ echo "selected";}?> value="7 pm">7 pm</option>
                                        <option <?php if($row2['timestart'] == "8 pm"){ echo "selected";}?> value="8 pm">8 pm</option>
                                        <option <?php if($row2['timestart'] == "9 pm"){ echo "selected";}?> value="9 pm">9 pm</option>
                                        <option <?php if($row2['timestart'] == "10 pm"){ echo "selected";}?> value="10 pm">10 pm</option>
                                        <option <?php if($row2['timestart'] == "11 pm"){ echo "selected";}?> value="11 pm">11 pm</option>
                                        <option <?php if($row2['timestart'] == "12 am"){ echo "selected";}?> value="12 am">12 am</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="timeend" class="form-control" id="day" placeholder="">
                                        <option value="" disabled>Ending Time</option>
                                        <option <?php if($row2['timeend'] == "9 am"){ echo "selected";}?> value="9 am">9 am</option>
                                        <option <?php if($row2['timeend'] == "10 am"){ echo "selected";}?> value="10 am">10 am</option>
                                        <option <?php if($row2['timeend'] == "11 am"){ echo "selected";}?> value="11 am">11 am</option>
                                        <option <?php if($row2['timeend'] == "12 pm"){ echo "selected";}?> value="12 pm">12 pm</option>
                                        <option <?php if($row2['timeend'] == "1 pm"){ echo "selected";}?> value="1 pm">1 pm</option>
                                        <option <?php if($row2['timeend'] == "2 pm"){ echo "selected";}?> value="2 pm">2 pm</option>
                                        <option <?php if($row2['timeend'] == "3 pm"){ echo "selected";}?> value="3 pm">3pm</option>
                                        <option <?php if($row2['timeend'] == "4 pm"){ echo "selected";}?> value="4 pm">4 pm</option>
                                        <option <?php if($row2['timeend'] == "5 pm"){ echo "selected";}?> value="5 pm">5 pm</option>
                                        <option <?php if($row2['timeend'] == "6 pm"){ echo "selected";}?> value="6 pm">6 pm</option>
                                        <option <?php if($row2['timeend'] == "7 pm"){ echo "selected";}?> value="7 pm">7 pm</option>
                                        <option <?php if($row2['timeend'] == "8 pm"){ echo "selected";}?> value="8 pm">8 pm</option>
                                        <option <?php if($row2['timeend'] == "9 pm"){ echo "selected";}?> value="9 pm">9 pm</option>
                                        <option <?php if($row2['timeend'] == "10 pm"){ echo "selected";}?> value="10 pm">10 pm</option>
                                        <option <?php if($row2['timeend'] == "11 pm"){ echo "selected";}?> value="11 pm">11 pm</option>
                                        <option <?php if($row2['timeend'] == "12 am"){ echo "selected";}?> value="12 am">12 am</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="quantity" class="col-sm-3 col-form-label">Maximum Patient</label>
                                <div class="col-sm-6">
                                    <input type="number" name="quantity" class="form-control" id="quantity" value="<?php echo $row2['quantity']; ?>" min="1" max="500" style="color: red;">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                <div class="col-sm-6">
                                    <select name="activity" class="form-control" id="" style="color: red;" >
                                        <option value="" disabled selected>Select Activity</option>
                                        <option <?php if($row2['activity'] == "Active"){ echo "selected";}?> value="Active">Active</option>
                                        <option <?php if($row2['activity'] == "Inactive"){ echo "selected";}?> value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="update" class="btn btn-success form-control">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>




