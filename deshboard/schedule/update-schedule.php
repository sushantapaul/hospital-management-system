<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['update'])) {
        $id = $_POST['id'];
        $name = $_POST['name'];
        $day = $_POST['day'];
        $timestart = $_POST['timestart'];
        $timeend = $_POST['timeend'];
        $quantity = $_POST['quantity'];
        $activity = $_POST['activity'];
        

        if(empty($name)) {
            $_SESSION['msz'] = "Name is required";
            header('Location: ./edit-schedule.php?id='.$id);
        }
        elseif(empty($day)) {
            $_SESSION['msz'] = "Day is required";
            header('Location: ./edit-schedule.php?id='.$id);
        }
        elseif(empty($timestart)) {
            $_SESSION['msz'] = "Time is required";
            header('Location: ./edit-schedule.php?id='.$id);
        }
        elseif(empty($timeend)) {
            $_SESSION['msz'] = "Time is required";
            header('Location: ./edit-schedule.php?id='.$id);
        }
        elseif(empty($quantity)) {
            $_SESSION['msz'] = "Patient quantity is required";
            header('Location: ./edit-schedule.php?id='.$id);
        }
        elseif(empty($activity)) {
            $_SESSION['msz'] = "Activity is required";
            header('Location: ./edit-schedule.php?id='.$id);
        } 
        else {    
            $sql = "UPDATE `schedule` SET `name`='$name',`day`='$day',`timestart`='$timestart',`timeend`='$timeend',`quantity`='$quantity', `activity`='$activity' WHERE id=$id";
        
            if($conn->query($sql) === TRUE) {
                $_SESSION['success'] = "Data Update Successfully";
                header('Location: ./schedule-list.php');
            }
        }
    } 
    else {
        header('Location: ./schedule-list.php');
    }

?>