<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>


<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Booking Bed</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="bed-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Bed List</a></li>
                            </div>
                        </div>
                        <form action="" method="">
                            <div class="form-group row">
                                <label for="roomno" class="col-sm-3 col-form-label">Room No</label>
                                <div class="col-sm-6">
                                    <input type="text" name="roomno" class="form-control" id="roomno" placeholder="Enter Room No" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bedno" class="col-sm-3 col-form-label">Bed No</label>
                                <div class="col-sm-6">
                                    <input type="text" name="bedno" class="form-control" id="bedno" placeholder="Enter Bed No" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="patientid" class="col-sm-3 col-form-label">Patient Id</label>
                                <div class="col-sm-6">
                                    <input type="text" name="" class="form-control" id="patientid" placeholder="Enter Patient Id">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="patientname" class="col-sm-3 col-form-label">Patient Name</label>
                                <div class="col-sm-6">
                                    <input name="" type="text" class="form-control" id="patientname" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bookingdate" class="col-sm-3 col-form-label">Booking Date</label>
                                <div class="col-sm-6">
                                    <input name="" type="date" class="form-control" id="bookingdate" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="button" name="" class="btn btn-success form-control">save</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="col-md-12 cn green" style="">Hospital Management System</div>
                    <!-- <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div> -->
                </div>
            </div>
        </footer>
    </div>
</div>


<?php include('../pertials_deshboard/footer.php')?>




