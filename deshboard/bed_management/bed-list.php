<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>


<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Bed Patients List</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="add-bed.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Booking Bed</a></li>
                            </div>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>Room No</th>
                                        <th>Bed No</th>
                                        <th>Patient Id</th>
                                        <th>Patient Name</th>
                                        <th>Booking Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Room No</td>
                                        <td>Bed No</td>
                                        <td>Patient Id</td>
                                        <td>Patient Name</td>
                                        <td>Booking Date</td>
                                        <td>
                                            <a href="" class="view"><i class="fas fa-eye"></i></a>
                                            <a href="" class="edit"><i class="fas fa-edit"></i></a>
                                            <a href="" class="delete"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Room No</td>
                                        <td>Bed No</td>
                                        <td>Patient Id</td>
                                        <td>Patient Name</td>
                                        <td>Booking Date</td>
                                        <td>
                                            <a href="" class="view"><i class="fas fa-eye"></i></a>
                                            <a href="" class="edit"><i class="fas fa-edit"></i></a>
                                            <a href="" class="delete"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>




