<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['update'])){

        $id = $_POST['id'];
        $name = $_POST['name'];
        $activity = $_POST['activity'];
        $cost = $_POST['cost'];

        if(empty($name)) {
            $_SESSION['msz'] = "Test Name is required!";
            header('Location: ./edit-test.php?id=' .$id);
        }
        elseif(empty($activity)) {
            $_SESSION['msz'] = "Activity is required!";
            header('Location: ./edit-test.php?id=' .$id);
        }
        elseif(empty($cost)) {
            $_SESSION['msz'] = "Cost is required!";
            header('Location: ./edit-test.php?id=' .$id);
        }
        else {
            $sql = "UPDATE `test` SET `name`='$name',`activity`='$activity',`cost`='$cost' WHERE id=$id";
            $result = $conn->query($sql);
            if($result === TRUE) {
                $_SESSION['success'] = "Information Updated Successfully!";
                header('location: ./test-list.php');
            } else {
                header('location: ./test-list.php');
            }
        }
    } else {
        header('Location: ./test-list.php');
    }

?>