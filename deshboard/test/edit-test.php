<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $id= $_GET['id'];
    if(empty($id)){
        header('Location: ./test-list.php');
    } else {
        $sql = "SELECT * FROM `test` WHERE id = $id";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        if(empty($row)){
            $_SESSION['msz'] = "No Data Found";
            header('Location: ./test-list.php');
        }
    }
?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Add Test</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                            <div class="pg-btn">
                                <div class="btn-item">
                                    <li><a href="test-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Test List</a></li>
                                </div>
                            </div>
                            <div class="alert-danger" role="alert">
                                <?php                                    
                                    if(isset($_SESSION['msz'])){
                                        echo $_SESSION['msz'];
                                        session_destroy();
                                    }                                
                                ?>
                            </div>
                            <div class="alert-success" role="alert">
                                <?php                                    
                                    if(isset($_SESSION['success'])){
                                        echo $_SESSION['success'];
                                        session_destroy();
                                    }                                
                                ?>
                            </div>
                            <form action="./update-test.php" method="POST">
                                <div class="form-group row">
                                    <input type="text" name="id" value="<?php echo $row['id']; ?>" style="display: none;">          
                                </div>
                                <div class="form-group row">
                                    <label for="testname" class="col-sm-3 col-form-label">Test Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="name" class="form-control" id="testname" value="<?php echo $row['name']; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                    <div class="col-sm-6">
                                        <select name="activity" class="form-control" id="activity" placeholder="">
                                            <option value="" disabled selected>Select Activity</option>
                                            <option <?php if($row['activity'] === "Active") { echo "selected"; }?> value="Active">Active</option>
                                            <option <?php if($row['activity'] === "Inactive") { echo "selected"; }?> value="Inactive">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cost" class="col-sm-3 col-form-label">Cost</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="cost" class="form-control" id="cost" value="<?php echo $row['cost']; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-6">
                                        <button type="submit" name="update" class="btn btn-success">update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>