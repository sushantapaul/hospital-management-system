<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $department = $_POST['department'];
        $speciality = $_POST['speciality'];
        $number = $_POST['number'];
        $nidnumber = $_POST['nidnumber'];
        $bloodgroup = $_POST['bloodgroup'];
        $email = $_POST['email'];
        $activity = $_POST['activity'];

        $img = $_FILES['images'];
        $img_name = $img['name'];

        if(empty($name)){
            $_SESSION['msz'] = "Name is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($gender)){
            $_SESSION['msz'] = "Gender is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($department)){
            $_SESSION['msz'] = "Department is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($speciality)){
            $_SESSION['msz'] = "Speciality is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($number)){
            $_SESSION['msz'] = "Number is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($nidnumber)){
            $_SESSION['msz'] = "NID Number is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($bloodgroup)){
            $_SESSION['msz'] = "Blood Group is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($email)){
            $_SESSION['msz'] = "Email is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($activity)){
            $_SESSION['msz'] = "Activity is required";
            header('location: ./add-doctor.php');
        }
        elseif(empty($img_name)){
            $_SESSION['msz'] = "Image is required";
            header('location: ./add-doctor.php');
        }
        else { 
            $sqldp = "SELECT * FROM `department` WHERE id=".$department; // department
            $result = $conn->query($sqldp);
            $row=$result->fetch_assoc();
            $depname = $row['depname'];


            $fileext = explode(".", $img_name); // image file
            $exten = strtolower(end($fileext));

            $allow = array('jpg', 'jpeg', 'png');

            if(in_array($exten, $allow)){
                $save = "../../dist/uploads/docimg/".$img_name;

                $upload = move_uploaded_file($img['tmp_name'], $save);

                if($upload) {
                    $sql = "INSERT INTO `doctor`(`name`, `gender`, `department`, `speciality`, `number`, `nidnumber`, `bloodgroup`, `email`, `activity`, `image`) VALUES ('$name','$gender', '$depname', '$speciality', '$number', '$nidnumber', '$bloodgroup', '$email', '$activity', '$img_name')";

                    if($conn->query($sql) === TRUE){
                        $_SESSION['success'] = "Successfully uploaded";
                        header('location: ./doctor-list.php');   
                    }
                }
            }
            else {
                $_SESSION['msz'] = "Image invalid";
                header('location: ./add-doctor.php');            
            }
        }
    }
    else {
        header('location: ./doctor-list.php');
    }
?>