<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php 
    include('../connect.php');
    session_start();

    $sql = "SELECT * FROM `doctor`";
    $result = $conn->query($sql);

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Doctors List</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="add-doctor.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Doctor</a></li>
                            </div>
                        </div>
                        <div class="alart alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div class="alart alert-success" role="alert">
                            <?php                                    
                                if(isset($_SESSION['success'])){
                                    echo $_SESSION['success'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Doctor's Name</th>
                                        <th>Department</th>
                                        <th>Speciality</th>
                                        <th>Contact</th>
                                        <th>Email Address</th>
                                        <th>Activity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php while($row = $result->fetch_assoc()){ ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $row['id']; ?></td>
                                            <td><?php echo ucwords($row['name']); ?></td>
                                            <td class="cn"><?php echo ucwords($row['department']); ?></td>
                                            <td class="cn"><?php echo ucwords($row['speciality']); ?></td>
                                            <td class="cn"><?php echo $row['number']; ?></td>
                                            <td class="cn"><?php echo $row['email']; ?></td>
                                            <td class="cn">
                                                <?php 
                                                    if($row['activity'] === 'inactive'){
                                                        echo "<span class='red'>" . ucwords($row['activity']) ."</span>";
                                                    } else {
                                                        echo "<span class='green'>" . ucwords($row['activity']) ."</span>";
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="./doctor-profile.php?id=<?php echo $row['id'];?>" class="view"><i class="fas fa-eye"></i></a>
                                                <a href="./edit-doctor.php?id=<?php echo $row['id'];?>" class="edit"><i class="fas fa-edit"></i></a>

                                                <form action="./delete-doctor.php" method="POST" style="display: inline;">
                                                    <input type="text" name="id" value="<?php echo $row['id'];?>" style="display: none;">
                                                    <button type="submit" name="delete" class="delete" style="border: none; background: #fff; padding: 0px;margin: 0px;" disabled><i class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>



