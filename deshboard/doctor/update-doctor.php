<?php
    session_start();
    include('../connect.php');


    if(isset($_POST['update'])){

        $id = $_POST['id'];
        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $department = $_POST['department'];  // +++
        $speciality = $_POST['speciality'];
        $number = $_POST['number'];
        $nidnumber = $_POST['nidnumber'];
        $bloodgroup = $_POST['bloodgroup'];
        $email = $_POST['email'];
        $activity = $_POST['activity'];

        $img = $_FILES['images'];
        $img_name = $img['name'];
    
        if(empty($name)){
            $_SESSION['msz'] = "Name is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($gender)){
            $_SESSION['msz'] = "gender is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($department)){
            $_SESSION['msz'] = "department is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($speciality)){
            $_SESSION['msz'] = "speciality is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($number)){
            $_SESSION['msz'] = "number is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($nidnumber)){
            $_SESSION['msz'] = "nidnumber is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($bloodgroup)){
            $_SESSION['msz'] = "bloodgroup is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($email)){
            $_SESSION['msz'] = "email is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($activity)){
            $_SESSION['msz'] = "activity is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        elseif(empty($img_name)){
            $_SESSION['msz'] = "image is required";
            header('location: ./edit-doctor.php?id=' .$id);
        }
        else {
            $sqldp = "SELECT * FROM `department` WHERE id=$department";
            $resultdp = $conn->query($sqldp);
            $rowdp = $resultdp->fetch_assoc();

            $dep = $rowdp['depname']; 

            $fileext = explode(".", $img_name);
            $exten = strtolower(end($fileext));

            $allow = array('jpg', 'jpeg', 'png');

            if(in_array($exten, $allow)){
                $save = "../../dist/uploads/docimg/".$img_name;

                $upload = move_uploaded_file($img['tmp_name'], $save);

                if($upload) {
                    $sql = "UPDATE `doctor` SET `name`='$name',`gender`='$gender',`department`='$dep',`speciality`='$speciality',`number`='$number',`nidnumber`='$nidnumber',`bloodgroup`='$bloodgroup',`email`='$email',`activity`='$activity',`image`='$img_name' WHERE id = $id";
                
                    if($conn->query($sql) === TRUE) {
                        $_SESSION['success'] = "Update successfully";
                        header('Location: ./doctor-list.php');
                    }
                }
            }
            else {
                $_SESSION['msz'] = "Image invalid";
                header('location: ./edit-doctor.php');            
            }
        }
    } 
    else {
        header('Location: ./edit-profile.php');
    }
?>