<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    // include('../connect.php');
    session_start();
?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Add Doctor</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="doctor-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Doctors List</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div class="alert-success" role="alert">
                            <?php 
                                if(isset($_SESSION['success'])){
                                    echo $_SESSION['success'];
                                    session_destroy();
                                }
                            ?>
                        </div>
                        <form action="./store-doctor.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="doctorname" class="col-sm-3 col-form-label">Doctor's Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="form-control" id="doctorname" placeholder="Enter Doctor Name" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                                <div class="col-sm-6">
                                    <select name="gender" class="form-control" id="gender">
                                        <option value="" disabled selected>Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="department" class="col-sm-3 col-form-label">Department</label>
                                <div class="col-sm-6">
                                    <select name="department" class="form-control" id="department">
                                        <option value="" disabled selected>Select Department</option>
                                            <?php 
                                                include('../connect.php');
                                                $sqldp = "SELECT * FROM `department`";
                                                $result = $conn->query($sqldp);

                                                while($row = $result->fetch_assoc()){ ?>
                                                    <option style="" value="<?php echo $row['id'];?>"><?php echo $row['depname'];?></option>
                                           <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="speciality" class="col-sm-3 col-form-label">Speciality</label>
                                <div class="col-sm-6">
                                    <select name="speciality" class="form-control" id="speciality" placeholder="" >
                                        <option value="" disabled selected>Select Speciality</option>
                                        <option value="cardiology">Cardiology</option>
                                        <option value="neurology">Neurology</option>
                                        <option value="gynaecology">Gynaecology</option>
                                        <option value="medicine">Medicine</option>
                                        <option value="child neurology">Child Neurology</option>
                                        <option value="skin / dermatology">Skin / Dermatology</option>
                                        <option value="eye / optharmology">Eye / Optharmology</option>
                                        <option value="medicine">Medicine</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="number" class="col-sm-3 col-form-label">Contact Number</label>
                                <div class="col-sm-6">
                                    <input name="number" type="text" class="form-control" id="number" placeholder="Enter Contact Number" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nidnumber" class="col-sm-3 col-form-label">NID Number</label>
                                <div class="col-sm-6">
                                    <input name="nidnumber" type="text" class="form-control" id="nidnumber" placeholder="Enter NID Number" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bloodgroup" class="col-sm-3 col-form-label">Blood Group</label>
                                <div class="col-sm-6">
                                    <select name="bloodgroup" class="form-control" id="bloodgroup" placeholder="" >
                                        <option value="" disabled selected>Select Blood Group</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email Address</label>
                                <div class="col-sm-6">
                                    <input name="email" type="email" class="form-control" id="email" placeholder="Enter Email Address" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                <div class="col-sm-6">
                                    <select name="activity" class="form-control" id="activity" placeholder="" >
                                        <option value="" disabled selected>Select Activity</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-sm-3 col-form-label">Your Image</label>
                                <div class="col-sm-6">
                                    <input type="file" name="images" class="form-control-file" id="image" placeholder="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="submit" class="btn btn-success form-control">save</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>


<?php include('../pertials_deshboard/footer.php')?>




