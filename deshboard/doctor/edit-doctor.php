<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>

<?php 
    session_start();
    include('../connect.php');

    $id = $_GET['id'];
    if(empty($id)){
        header('Location: ./doctor-list.php');
    } else {
        $sql = "SELECT * FROM `doctor` WHERE id = $id";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

       // $dep = $row['department'];

        if(empty($row)){
            $_SESSION['msz'] = "No Data Found";
            header('Location: ./doctor-list.php');
        }
    }
    // $sql2 = "SELECT * FROM `department` WHERE depname=$dep";
    // $result2 = $conn->query($sql2); 

    
?>


<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Add Doctor</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="doctor-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Doctors List</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                       
                            ?>
                        </div>
                        <form action="./update-doctor.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <input type="text" name="id" value="<?php echo $row['id']; ?>" style="display: none;">          
                            </div>
                            <div class="form-group row">
                                <label for="doctorname" class="col-sm-3 col-form-label">Doctor's Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="form-control cap" id="doctorname" value="<?php echo $row['name']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                                <div class="col-sm-6">
                                    <select name="gender" class="form-control" id="gender" >
                                        <option value=""  disabled>Select Gender</option>
                                        <option <?php if($row['gender']=="male"){echo "selected";}?> value="male">Male</option>
                                        <option <?php if($row['gender']=="female"){echo "selected";}?> value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="department" class="col-sm-3 col-form-label">Department</label>
                                <div class="col-sm-6">
                                    <select name="department" class="form-control" id="department">
                                        <?php 
                                            include('../connect.php');
                                            $sqldp = "SELECT * FROM `department`";
                                            $result = $conn->query($sqldp);

                                            while($rownew = $result->fetch_assoc()){ ?>
                                                <option value="<?php echo $rownew['id'];?>" <?php echo ($rownew['depname'] == $row['department']?'selected':''); ?>><?php echo $rownew['depname'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="speciality" class="col-sm-3 col-form-label">Speciality</label>
                                <div class="col-sm-6">
                                    <select name="speciality" class="form-control" id="speciality" placeholder="" >
                                        <option value="" disabled>Select Speciality</option>
                                        <option <?php if($row['speciality']=="cardiology"){echo "selected";}?> value="cardiology">Cardiology</option>
                                        <option <?php if($row['speciality']=="neurology"){echo "selected";}?> value="neurology">Neurology</option>
                                        <option <?php if($row['speciality']=="gynaecology"){echo "selected";}?> value="gynaecology">Gynaecology</option>
                                        <option <?php if($row['speciality']=="medicine"){echo "selected";}?> value="medicine">Medicine</option>
                                        <option <?php if($row['speciality']=="child neurology"){echo "selected";}?> value="child neurology">Child Neurology</option>
                                        <option <?php if($row['speciality']=="skin / dermatology"){echo "selected";}?> value="skin / dermatology">Skin / Dermatology</option>
                                        <option <?php if($row['speciality']=="eye / optharmology"){echo "selected";}?> value="eye / optharmology">Eye / Optharmology</option>
                                        <option <?php if($row['speciality']=="medicine"){echo "selected";}?> value="medicine">Medicine</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="number" class="col-sm-3 col-form-label">Contact Number</label>
                                <div class="col-sm-6">
                                    <input name="number" type="text" class="form-control" id="number" value="<?php echo $row['number']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nidnumber" class="col-sm-3 col-form-label">NID Number</label>
                                <div class="col-sm-6">
                                    <input name="nidnumber" type="text" class="form-control" id="nidnumber" value="<?php echo $row['nidnumber']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bloodgroup" class="col-sm-3 col-form-label">Blood Group</label>
                                <div class="col-sm-6">
                                    <select name="bloodgroup" class="form-control" id="bloodgroup" placeholder="" >
                                        <option value="" disabled>Select Blood Group</option>
                                        <option <?php if($row['bloodgroup']=="A+"){echo "selected";} ?> value="A+">A+</option>
                                        <option <?php if($row['bloodgroup']=="A-"){echo "selected";} ?> value="A-">A-</option>
                                        <option <?php if($row['bloodgroup']=="B+"){echo "selected";} ?> value="B+">B+</option>
                                        <option <?php if($row['bloodgroup']=="B-"){echo "selected";} ?> value="B-">B-</option>
                                        <option <?php if($row['bloodgroup']=="O+"){echo "selected";} ?> value="O+">O+</option>
                                        <option <?php if($row['bloodgroup']=="O-"){echo "selected";} ?> value="O-">O-</option>
                                        <option <?php if($row['bloodgroup']=="AB+"){echo "selected";} ?> value="AB+">AB+</option>
                                        <option <?php if($row['bloodgroup']=="AB-"){echo "selected";} ?> value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email Address</label>
                                <div class="col-sm-6">
                                    <input name="email" type="email" class="form-control" id="email" value="<?php echo $row['email']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                <div class="col-sm-6">
                                    <select name="activity" class="form-control" id="activity" placeholder="" >
                                        <option value="" disabled>Select Activity</option>
                                        <option <?php if($row['activity'] == "active"){echo "selected";} ?> value="active">Active</option>
                                        <option <?php if($row['activity'] == "inactive"){echo "selected";} ?> value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-sm-3 col-form-label">Your Image</label>
                                <div class="col-sm-3">
                                    <img class="img-thumbnail" src="../../dist/uploads/docimg/<?php echo $row['image']?>" alt="" style="width:100px;height:100px;float:left;">
                                </div>
                                <div class="col-sm-3">
<input name="images" type="file" class="form-group  form-control-file" id="image" value="../../dist/uploads/docimg/<?php echo $row['image']; ?>" >

                                    <button type="submit" name="update" class="btn btn-success form-control">Update</button>
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <!-- <button type="submit" name="update" class="btn btn-success form-control">Update</button> -->
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>



<?php include('../pertials_deshboard/footer.php')?>




