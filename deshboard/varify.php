<?php
    include('./connect.php');
    session_start();

    if(isset($_POST['submit'])){

        $email = $_POST['email'];
        $password = $_POST['password'];

        if(empty($email)){
            $_SESSION['msz'] = "Email is required";
            header('location: ./login.php');
        }
        elseif(empty($password)){
            $_SESSION['msz'] = "Password is required";
            header('location: ./login.php');
        }
        else {
            $sql = "SELECT `id`, `name`, `usertype`, `email`, `password` FROM `user` WHERE email= '$email'";

            $result = $conn->query($sql);

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()){
                    if(password_verify($password, $row['password'])){
                        $_SESSION['username'] = $row['name'];
                        $_SESSION['userid'] = $row['id'];

                        header('location: ./index.php');
                    } else {
                        $_SESSION['msz'] = "Invalid password";
                        header('location: ./login.php');
                    }
                }
            } else {
                $_SESSION['msz'] = "Invalid email";
                header('location: ./login.php');
            }
        }
    } else {
        $_SESSION['msz'] = "Please login your account";
        header('location: ./login.php');
    }



?>