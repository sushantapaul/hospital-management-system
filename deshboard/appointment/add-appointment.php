<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    // include('../connect.php');
    session_start();
?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Add Appointment</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                            <div class="pg-btn">
                                <div class="btn-item">
                                    <li><a href="appointment-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Appointment List</a></li>
                                </div>
                            </div>
                            <div class="alert-danger" role="alert">
                                <?php                                    
                                    if(isset($_SESSION['msz'])){
                                        echo $_SESSION['msz'];
                                        session_destroy();
                                    }                       
                                ?>
                            </div>
                            <form action="./store-appointment.php" method="POST">
                                <div class="form-group row">
                                    <label for="patientid" class="col-sm-3 col-form-label">Patient ID</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="patientid" class="form-control" id="patientid" placeholder="Enter Patient ID">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="type" class="col-sm-3 col-form-label">Appointment Type</label>
                                    <div class="col-sm-6">
                                        <select name="type" class="form-control" id="type" placeholder="">
                                            <option value="" disabled selected>Select Patient Type</option>
                                            <option value="new">New</option>
                                            <option value="old">Old</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="date" class="col-sm-3 col-form-label">Appointment Date</label>
                                    <div class="col-sm-6">
                                        <input type="date" name="date" class="form-control" id="date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="schedulid" class="col-sm-3 col-form-label">Scheduls Id</label>
                                    <div class="col-sm-6">
                                    <input type="text" name="schedulid" class="form-control" id="schedulid" placeholder="Enter Scheduls Id">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="activity" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-6">
                                    <button type="submit" name="submit" class="btn btn-success form-control">save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>



<?php include('../pertials_deshboard/footer.php')?>




