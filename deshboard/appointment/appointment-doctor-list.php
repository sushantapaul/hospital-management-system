<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $sql = "SELECT schedule.id, schedule.name, schedule.day, schedule.timestart, schedule.timeend, schedule.quantity, schedule.activity, doctor.speciality FROM schedule, doctor 
    WHERE schedule.name = doctor.name ORDER BY schedule.id";

    $result = $conn->query($sql);

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Chose Doctor for Appointment</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="./appointment-list.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Appointment List</a></li>
                            </div>
                            <div class="btn-item">
                                <li><a href="../patient/add-patient.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add patient</a></li>
                            </div>
                        </div>                        
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div class="alert-success" role="alert">
                            <?php                                    
                                if(isset($_SESSION['success'])){
                                    echo $_SESSION['success'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Doctor Name</th>
                                        <th>Speciality</th>
                                        <th>Available day</th>
                                        <th>Available Time</th>
                                        <th>Max Patient</th>
                                        <th>Activity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php while($row = $result->fetch_assoc()) { ?>
                                <tbody>
                                    <tr>
                                        <td><?php echo $row['id']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td class="cn"><?php echo $row['speciality']; ?></td>
                                        <td class="cn"><?php echo $row['day']; ?></td>
                                        <td class="cn"><?php echo $row['timestart']." to ".$row['timeend']; ?></td>
                                        <td class="cn"><?php echo $row['quantity']; ?></td>
                                        <!-- <td><?php // echo $row['activity']; ?></td> -->
                                        <td class="cn"><?php 
                                                if($row['activity'] == "Inactive") {
                                                    echo "<span style='color: red;'>".$row['activity']."</span>";
                                                } else {
                                                    echo "<span style='color: green;'>".$row['activity']."</span>";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="./appointment.php?id=<?php echo $row['id']?>" class="view"><i class="fa fa-user-md" aria-hidden="true"></i></a>
                                            <!-- <a href="#" class="edit"><i class="fas fa-edit"></i></a>
                                            <a href="#" class="delete" ><i class="fas fa-trash-alt"></i></a> -->
                                        </td>
                                    </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>                        
                    </div>  
                </div>
            </section>          
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>

