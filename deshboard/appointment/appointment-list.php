<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $sql = "SELECT * FROM `appoinment`";
    $result = $conn->query($sql);

?>
<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Appointment List</h1>
                    </div>
                </div>
            </section >
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="../patient/add-patient.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Patient</a></li>
                            </div>
                            <div class="btn-item">
                                <li><a href="../appointment/appointment-doctor-list.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Appointment</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                  
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }
                            ?>
                        </div>
                        <div class="alert-success" role="alert">
                            <?php                                    
                                if(isset($_SESSION['success'])){
                                    echo $_SESSION['success'];
                                    session_destroy();
                                }                              
                            ?>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Patient's Name</th>
                                        <th>Type</th>
                                        <th>Mobile</th>
                                        <th>Date</th>
                                        <th>Day</th>
                                        <th>Dr Name</th>
                                    </tr>
                                </thead>
                                <?php while($row=$result->fetch_assoc()) { ?>
                                <tbody>
                                    <tr>
                                        <td><?php echo $row['id']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td class="cn"><?php echo $row['type']; ?></td>
                                        <td class="cn"><?php echo $row['number']; ?></td>
                                        <td class="cn"><?php $m = explode("-", $row['date']);
                                                $p = $m[2]."-".$m[1]."-".$m[0];
                                                echo $p;
                                        ?></td>
                                        <td class="cn"><?php echo $row['day']; ?></td>
                                        <td><?php echo $row['docname']; ?></td>
                                    </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>