<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])){
        $id = $_POST['id'];
        $patientid = $_POST['patientid'];
        $type = $_POST['type'];
        $date = $_POST['date'];
        $day = $_POST['day'];
        $docname = $_POST['docname'];

        if(empty($patientid)){
            $_SESSION['msz'] = "Patient Id is required";
            header('Location: ./appointment.php?id='.$id);
        }
        elseif(empty($type)){
            $_SESSION['msz'] = "Type is required";
            header('Location: ./appointment.php?id='.$id);
        }
        elseif(empty($date)){
            $_SESSION['msz'] = "Date is required";
            header('Location: ./appointment.php?id='.$id);
        }
        else {
            $sqlp = "SELECT * FROM `patient` WHERE id=$patientid";
            $resultp = $conn->query($sqlp);
            $rowp=$resultp->fetch_assoc();
        
            $patientname = $rowp['patientName'];
            $number = $rowp['number'];

            if(empty($rowp)){
                $_SESSION['msz'] = "Invalid Patient ID";
                header('Location: ./appointment.php?id='.$id);
            } else {
                $sql = "INSERT INTO `appoinment`(`name`, `type`, `number`, `date`, `day`, `docname`) VALUES ('$patientname','$type','$number','$date','$day','$docname')";
                if($conn->query($sql) === TRUE) {
                    $_SESSION['success'] = "Appointment Successful";
                    header('Location: ./appointment-list.php');
                }
            }
        }
    } else {
        header('Location: ./appointment-doctor-list.php');
    }


?>