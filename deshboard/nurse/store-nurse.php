<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])) {
        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $number = $_POST['number'];
        $nid = $_POST['nid'];
        $bloodgroup = $_POST['bloodgroup'];
        $activity = $_POST['activity'];

        $image = $_FILES['image'];
        $img_name = $image['name'];

        if(empty($name)){
            $_SESSION['msz'] = "Name is required";
            header('location: ./add-nurse.php');
        }
        elseif(empty($gender)){
            $_SESSION['msz'] = "Gender is required";
            header('location: ./add-nurse.php');
        }
        elseif(empty($number)){
            $_SESSION['msz'] = "Number is required";
            header('location: ./add-nurse.php');
        }
        elseif(empty($nid)){
            $_SESSION['msz'] = "NID is required";
            header('location: ./add-nurse.php');
        }
        elseif(empty($bloodgroup)){
            $_SESSION['msz'] = "Blood Group is required";
            header('location: ./add-nurse.php');
        }
        elseif(empty($activity)){
            $_SESSION['msz'] = "Activity is required";
            header('location: ./add-nurse.php');
        }
        elseif(empty($img_name)){
            $_SESSION['msz'] = "Image is required";
            header('location: ./add-nurse.php');
        }
        else {
            $fileext = explode(".", $img_name);
            $ext = end($fileext);

            $allow = array('jpg', 'jpeg', 'png');

            if(in_array($ext, $allow)) {
                $save = "../../dist/uploads/nurse/".$img_name;
                $upload = move_uploaded_file($image['tmp_name'], $save);

                if($upload){
                    $sql = "INSERT INTO `nurse`(`name`, `gender`, `number`, `nid`, `bloodgroup`, `activity`, `image`) VALUES ('$name', '$gender', '$number', '$nid', '$bloodgroup', '$activity', '$img_name')";

                    if($conn->query($sql) === TRUE) {
                        $_SESSION['success'] = "Profile upload successfully";
                        header('location: ./nurse-list.php');
                    }
                }
            }
            else {
                $_SESSION['msz'] = "Image invalid";
                header('location: ./add-nurse.php');    
            }
        }
    }
    else {
        header('location: ./nurse-list.php');
    }

?>