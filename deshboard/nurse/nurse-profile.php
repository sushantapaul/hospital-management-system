<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $id=$_GET['id'];
    if(empty($id)){
        header('Location: ./nurse-list.php');
    }
    else {
        $sql = "SELECT * FROM `nurse` WHERE id=$id";
        $result = $conn->query($sql);
        $row=$result->fetch_assoc();
        if(empty($row)){
            header('Location: ./nurse-list.php');
        }
    }
?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Nurses Profile</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="nurse-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Nurses List</a></li>
                            </div>
                            <div class="btn-item">
                                <li><a href="add-nurse.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Nurse</a></li>
                            </div>
                        </div>
                        <div class="info-headline mb-4">
                            <h3>Nurses Information</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="id-img">
                                    <img class="img-thumbnail" src="../../dist/uploads/nurse/<?php echo $row['image']?>" alt="" style="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="id-view">
                                    <table class="table new-table">
                                        <tr>
                                            <td class="left-tbl">Full Name</td>
                                            <td class="right-tbl cap"><?php echo $row['name'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Gander</td>
                                            <td class="right-tbl cap"><?php echo $row['gender'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Contact Number</td>
                                            <td class="right-tbl"><?php echo $row['number'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">NID Number</td>
                                            <td class="right-tbl"><?php echo $row['nid'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Blood Group</td>
                                            <td class="right-tbl"><?php echo $row['bloodgroup'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Activity</td>
                                            <td class="right-tbl cap"><?php if($row['activity'] === "inactive") { echo "<span class='red'>".$row['activity']."</span>";}else{ echo "<span class='green'>".$row['activity']."</span>";}?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>


<?php include('../pertials_deshboard/footer.php')?>




