<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $sql = "SELECT * FROM `nurse`";
    $result = $conn->query($sql);

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Nurses List</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="add-nurse.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Nurse</a></li>
                            </div>
                        </div>
                        <div class="alert-success" role="alert">
                            <?php 
                                if(isset($_SESSION['success'])){
                                    echo $_SESSION['success'];
                                    session_destroy();
                                }
                            ?>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nurse Name</th>
                                        <th>Gender</th>
                                        <th>Contact</th>
                                        <th>NID Number</th>
                                        <th>Blood Group</th>
                                        <th>Activity</th>
                                        <!-- <th>Image</th> -->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php while($row=$result->fetch_assoc()) { ?>
                                <tbody>
                                    <tr>
                                        <td><?php echo $row['id'];?></td>
                                        <td class="cap"><?php echo $row['name'];?></td>
                                        <td class="cap cn"><?php echo $row['gender'];?></td>
                                        <td class="cn"><?php echo $row['number'];?></td>
                                        <td class="cn"><?php echo $row['nid'];?></td>
                                        <td class="cn"><?php echo $row['bloodgroup'];?></td>
                                        <td class="cap cn"><?php if($row['activity'] === "inactive") { echo "<span class='red'>".$row['activity']."</span>";}else{ echo "<span class='green'>".$row['activity']."</span>";}?></td>
                                        <!-- <td class=""><img class="img-thumbnail" src="../../dist/uploads/nurse/<?php // echo $row['image']?>" alt="" style="width: 50px;height: 50px;"></td> -->
                                        <td>
                                            <a href="./nurse-profile.php?id=<?php echo $row['id'];?>" class="view"><i class="fas fa-eye"></i></a>
                                            <a href="" class="edit"><i class="fas fa-edit"></i></a>
                                            <a href="" class="delete"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                </tbody>

                                <?php }?>
                            </table>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>




