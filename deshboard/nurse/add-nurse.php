<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php 
    include('../connect.php');
    session_start(); 
?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Add Nurse</h1>
                    </div>
                </div>
            </section >
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="nurse-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Nurses List</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <form action="./store-nurse.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Nurse Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Nurse Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="gender" id="gender">
                                        <option value="" disabled selected>Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="number" class="col-sm-3 col-form-label">Contact Number</label>
                                <div class="col-sm-6">
                                    <input type="text" name="number" class="form-control" id="number" placeholder="Enter Contact Number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nidnumber" class="col-sm-3 col-form-label">NID Number</label>
                                <div class="col-sm-6">
                                    <input type="text" name="nid" class="form-control" id="nidnumber" placeholder="Enter NID Number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bloodgroup" class="col-sm-3 col-form-label">Blood Group</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="bloodgroup" id="bloodgroup">
                                        <option value="" disabled selected>Select Blood Group</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="activity" id="activity">
                                        <option value="" disabled selected>Select Activity</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-sm-3 col-form-label">Your Image</label>
                                <div class="col-sm-6">
                                    <input type="file" name="image" class="form-control-file" id="image" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="submit" class="btn btn-success form-control">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>



<?php include('../pertials_deshboard/footer.php')?>




