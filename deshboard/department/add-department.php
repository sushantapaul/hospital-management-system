<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    // include('../connect.php');
    session_start();

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Add Department</h1>
                    </div>
                </div>
            </section >
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li>
                                    <a href="department-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Department List</a>
                                </li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <form action="./store-department.php" method="POST">
                            <div class="form-group row">
                                <label for="depname" class="col-sm-3 col-form-label">Department Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="depname" class="form-control" id="depname" placeholder="Enter Department Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label">Activity</label>
                                <div class="col-sm-6">
                                    <select name="activity" class="form-control" id="activity">
                                        <option value="" disabled selected>Select Activity</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="submit" class="btn btn-success form-control">save</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>




<?php include('../pertials_deshboard/footer.php')?>