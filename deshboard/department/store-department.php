<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])) {
        $depname = $_POST['depname'];
        $activity = $_POST['activity'];
        if(empty($depname)){
            $_SESSION['msz'] = "Department name is required";
            header('Location: ./add-department.php');
        }
        elseif(empty($activity)){
            $_SESSION['msz'] = "Activity is required";
            header('Location: ./add-department.php');
        }
        else {
            $sql = "INSERT INTO `department`(`depname`, `activity`) VALUES ('$depname','$activity')";

            if($conn->query($sql) === TRUE) {
                $_SESSION['success'] = "Data Upload Successfully";
                header('Location: ./department-list.php');
            }
        }
    }
    else {
        header('Location: ./department-list.php');
    }
    

?>