<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $sql = "SELECT * FROM `patient`";
    $result = $conn->query($sql);

?>
<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Patients List</h1>
                    </div>
                </div>
            </section >
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="add-patient.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Patient</a></li>
                            </div>
                            <div class="btn-item">
                                <li><a href="../appointment/appointment-doctor-list.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Appointment</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php                                    
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div class="alert-success" role="alert">
                            <?php                                    
                                if(isset($_SESSION['success'])){
                                    echo $_SESSION['success'];
                                    session_destroy();
                                }                                
                            ?>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Patient's Name</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Blood Group</th>
                                        <th>Guardian Name</th>
                                        <th>Contact Number</th>
                                        <th>Admission Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php while($row = $result->fetch_assoc()) { ?>
                                <tbody>
                                    <tr>
                                        <td><?php echo $row['id']; ?></td>
                                        <td class="cap"><?php echo $row['patientName']; ?></td>
                                        <td class="cn cap"><?php echo $row['gender']; ?></td>
                                        <td class="cn"><?php echo $row['age']; ?></td>
                                        <td class="cn"><?php echo $row['bloodgroup']; ?></td>
                                        <td class="cap"><?php echo $row['guardianName']; ?></td>
                                        <td class="cn"><?php echo $row['number']; ?></td>
                                        <td class="cn"><?php 
                                            $n = explode("-", $row['date']);
                                            $m = $n[2]."-".$n[1]."-".$n[0];
                                            echo $m;
                                        ?></td>
                                        <td>
                                            <a href="./patient-profile.php?id=<?php echo $row['id']; ?>" class="view"><i class="fas fa-eye"></i></a>
                                            <a href="./edit-patient.php?id=<?php echo $row['id']; ?>" class="edit"><i class="fas fa-edit"></i></a>
                                            <form action="./delete-patient.php" method="POST" style="display: inline;">
                                                <input type="text" name="id" value="<?php echo $row['id']; ?>" style="display: none;">
                                                <button type="submit" name="delete" class="delete" style="border: none; background: #fff; padding: 0px;margin: 0px;" disabled><i class="fas fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>