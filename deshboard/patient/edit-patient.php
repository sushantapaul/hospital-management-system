<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>

<?php 
    session_start();
    include('../connect.php');

    $id = $_GET['id'];

    if(empty($id)){
        header('Location: ./patient-list.php');
    } else {
        $sql = "SELECT * FROM `patient` WHERE id = $id";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        if(empty($row)){
            $_SESSION['msz'] = "No Data Found";
            header('Location: ./patient-list.php');
        }
    }

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Edit Patients Information</h1>
                    </div>
                </div>
            </section >
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="./patient-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Patient List</a></li>
                            </div>
                        </div>
                        <div class="alert-danger" role="alert">
                            <?php
                                if(isset($_SESSION['msz'])){
                                    echo $_SESSION['msz'];
                                    session_destroy();
                                }      
                            ?>
                        </div>
                        <form action="./update-patient.php" method="post">
                            <div class="form-group row">
                                <input type="text" name="id" value="<?php echo $row['id']; ?>" style="display: none;">          
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Patient Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="patientName" class="form-control" id="firstname" value="<?php echo $row['patientName']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="gender" id="gender"  >
                                        <option value="" disabled selected>Select</option>
                                        <option <?php if($row['gender'] == "male") { echo "selected"; }?> value="male">Male</option>
                                        <option <?php if($row['gender'] == "female") { echo "selected"; }?> value="female">Female</option>
                                        <option <?php if($row['gender'] == "other") { echo "selected"; }?> value="other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="age" class="col-sm-3 col-form-label">Age</label>
                                <div class="col-sm-6">
                                    <input type="text" name="age" class="form-control" id="age" value="<?php echo $row['age']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="bloodgroup" class="col-sm-3 col-form-label">Blood Group</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="bloodgroup" id="bloodgroup" >
                                        <option value="" disabled selected>Select Blood Group</option>
                                        <option <?php if($row['bloodgroup'] == "A+") { echo "selected"; }?> value="A+">A+</option>
                                        <option <?php if($row['bloodgroup'] == "A-") { echo "selected"; }?> value="A-">A-</option>
                                        <option <?php if($row['bloodgroup'] == "B+") { echo "selected"; }?> value="B+">B+</option>
                                        <option <?php if($row['bloodgroup'] == "B-") { echo "selected"; }?> value="B-">B-</option>
                                        <option <?php if($row['bloodgroup'] == "O+") { echo "selected"; }?> value="O+">O+</option>
                                        <option <?php if($row['bloodgroup'] == "O-") { echo "selected"; }?> value="O-">O-</option>
                                        <option <?php if($row['bloodgroup'] == "AB+") { echo "selected"; }?> value="AB+">AB+</option>
                                        <option <?php if($row['bloodgroup'] == "AB-") { echo "selected"; }?> value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="guardian" class="col-sm-3 col-form-label">Guardian Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="guardianName" class="form-control" id="guardian" value="<?php echo $row['guardianName']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="number" class="col-sm-3 col-form-label">Contact Number</label>
                                <div class="col-sm-6">
                                    <input type="tel" name="number" class="form-control" id="number" value="<?php echo $row['number']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-sm-3 col-form-label">Admission Date</label>
                                <div class="col-sm-6">
                                    <input type="date" name="date" class="form-control" id="date" value="<?php echo $row['date']; ?>" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="update" class="btn btn-success form-control">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>
<?php // include('../pertials-deshboard/footer.php')?>