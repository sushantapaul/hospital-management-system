<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['update'])) {
        $id = $_POST['id'];
        $patientName = $_POST['patientName'];
        $gender = $_POST['gender'];
        $age = $_POST['age'];
        $bloodgroup = $_POST['bloodgroup'];
        $guardianName = $_POST['guardianName'];
        $number = $_POST['number'];
        $date = $_POST['date'];

        if(empty($patientName)) {
            $_SESSION['msz'] = "Name is requird";
            header('location: ./edit-patient.php?id=' .$id);
        }
        elseif(empty($gender)) {
            $_SESSION['msz'] = "Gender is requird";
            header('location: ./edit-patient.php?id=' .$id);
        }
        elseif(empty($age)) {
            $_SESSION['msz'] = "Age is requird";
            header('location: ./edit-patient.php?id=' .$id);
        }
        elseif(empty($bloodgroup)) {
            $_SESSION['msz'] = "Bloodgroup is requird";
            header('location: ./edit-patient.php?id=' .$id);
        }
        elseif(empty($guardianName)) {
            $_SESSION['msz'] = "Guardian Name is requird";
            header('location: ./edit-patient.php?id=' .$id);
        }
        elseif(empty($date)) {
            $_SESSION['msz'] = "Admission Date is requird";
            header('location: ./edit-patient.php?id=' .$id);
        }
        else {
            $sql = "UPDATE `patient` SET `patientName`='$patientName',`gender`='$gender',`age`='$age',`bloodgroup`='$bloodgroup',`guardianName`='$guardianName',`number`='$number',`date`='$date' WHERE id=$id";

            if($conn->query($sql) === TRUE) {
                $_SESSION['success'] = "Information Updated Successfully!";
                header('location: ./patient-list.php');
            } else {
                header('location: ./patient-list.php');
            }
        }
    }
    else {
        header('location: ./patient-list.php');
    }

?>