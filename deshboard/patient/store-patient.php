<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])) {
        $patientName = $_POST['patientName'];
        $gender = $_POST['gender'];
        $age = $_POST['age'];
        $bloodgroup = $_POST['bloodgroup'];
        $guardianName = $_POST['guardianName'];
        $number = $_POST['number'];
        $date = $_POST['date'];

        if(empty($patientName)) {
            $_SESSION['msz'] = "Name is requird";
            header('location: ./add-patient.php');
        }
        elseif(empty($gender)) {
            $_SESSION['msz'] = "Gender is requird";
            header('location: ./add-patient.php');
        }
        elseif(empty($age)) {
            $_SESSION['msz'] = "Age is requird";
            header('location: ./add-patient.php');
        }
        elseif(empty($bloodgroup)) {
            $_SESSION['msz'] = "Bloodgroup is requird";
            header('location: ./add-patient.php');
        }
        elseif(empty($guardianName)) {
            $_SESSION['msz'] = "Guardian Name is requird";
            header('location: ./add-patient.php');
        }
        elseif(empty($date)) {
            $_SESSION['msz'] = "Date is requird";
            header('location: ./add-patient.php');
        }
        else {
            $sql = "INSERT INTO `patient`(`patientName`, `gender`, `age`, `bloodgroup`, `guardianName`, `number`, `date`) VALUES ('$patientName', '$gender', '$age', '$bloodgroup', '$guardianName', '$number', '$date')";

            if($conn->query($sql) === TRUE) {
                $_SESSION['success'] = "Information Create Successfully!";
                header('location: ./patient-list.php');
            } else {
                header('location: ./patient-list.php');
            }
        }
    }
    else {
        header('location: ./patient-list.php');
    }

?>