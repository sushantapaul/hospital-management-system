<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');
    session_start();

    $id = $_GET['id'];
    
    if(empty($id)){
        header('Location: ./patient-list.php');
    } else {
        $sql = "SELECT * FROM `patient` WHERE id = $id";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        if(empty($row)){
            $_SESSION['msz'] = "No Data Found";
            header('Location: ./patient-list.php');
        }
    }

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Patients Profile</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="./patient-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Patients List</a></li>
                            </div>
                            <div class="btn-item">
                                <li><a href="./add-patient.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add Patient</a></li>
                            </div>
                        </div>
                        <div class="info-headline mb-4">
                            <h3>Patients Details</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="id-view">
                                    <table class="table new-table">
                                        <tr>
                                            <td class="left-tbl">Patient's Name</td>
                                            <td class="right-tbl cap"><?php echo $row['patientName']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Patient's Id</td>
                                            <td class="right-tbl"><?php echo $row['id']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Gander</td>
                                            <td class="right-tbl cap"><?php echo $row['gender']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Age</td>
                                            <td class="right-tbl"><?php echo $row['age']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Blood Group</td>
                                            <td class="right-tbl"><?php echo $row['bloodgroup']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Guardian Name</td>
                                            <td class="right-tbl cap"><?php echo $row['guardianName']; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Contact Number</td>
                                            <td class="right-tbl"><?php echo $row['number']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>


<?php include('../pertials_deshboard/footer.php')?>