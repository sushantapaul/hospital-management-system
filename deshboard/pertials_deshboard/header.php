<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard</title>
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <?php $base = 'http://'. $_SERVER['HTTP_HOST'] . '/'; ?>
   
    <?php echo '<link href="' . $base . 'hospital_management_system/dist/css/styles.css" rel="stylesheet">'; ?>
    <?php echo '<link href="' . $base . 'hospital_management_system/dist/css/custom.css" rel="stylesheet">'; ?>
    <?php echo '<link href="' . $base . 'hospital_management_system/dist/assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet">'; ?>
    <?php echo '<link href="' . $base . 'hospital_management_system/dist/assets/lib/chosen/chosen.css" rel="stylesheet">'; ?>
</head>
<body class="sb-nav-fixed">