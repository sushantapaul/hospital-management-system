<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                
                <a class="nav-link" href="<?php echo $base.'hospital_management_system/deshboard/index.php' ?>">
                    <div class="sb-nav-link-icon">
                    <i class="fas fa-clinic-medical"></i>
                    </div>
                    Dashboard
                </a>
                <div class="b-btm"></div>

                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/user/user-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                    <i class="fas fa-user-alt"></i>
                    </div>
                    User 
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/department/department-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fa fa-list-ol" aria-hidden="true"></i>
                    </div>
                    Department 
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/doctor/doctor-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fa fa-user-md" aria-hidden="true"></i>
                    </div>
                    Doctor
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/patient/patient-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fa fa-wheelchair" aria-hidden="true"></i>
                    </div>
                    Patient
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/appointment/appointment-doctor-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                    <i class="far fa-edit"></i>
                    </div>
                    Appointment 
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/schedule/schedule-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                    <i class="far fa-address-card"></i>
                    </div>
                    Schedule 
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/test/test-list.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                    <i class="fas fa-flask"></i>
                    </div>
                    Test 
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/billing/billing.php' ?>" data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fa fa-calculator" aria-hidden="true"></i>
                    </div>
                    Billing 
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/nurse/nurse-list.php' ?>"
                data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                    <i class="fas fa-user-nurse"></i>
                    </div>
                    Nurse
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/bed_management/bed-list.php' ?>" 
                data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fa fa-bed" aria-hidden="true"></i>
                    </div>
                    Bed Management
                </a>
                <a class="nav-link collapsed" href="<?php echo $base.'hospital_management_system/deshboard/messages/message-list.php' ?>" 
                data-toggle="" data-target="" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                    Messages
                </a>
                
                <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-columns"></i>
                    </div>
                    Layouts
                    <div class="sb-sidenav-collapse-arrow">
                        <i class="fas fa-angle-down"></i>
                    </div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="layout-static.html">Static Navigation</a>
                        <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                    </nav>
                </div> -->


                <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-book-open"></i>
                    </div>
                    Pages
                    <div class="sb-sidenav-collapse-arrow">
                        <i class="fas fa-angle-down"></i>
                        </div>
                </a>
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth" > Authentication
                            <div class="sb-sidenav-collapse-arrow">
                                <i class="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="login.html">Login</a>
                                <a class="nav-link" href="register.html">Register</a>
                                <a class="nav-link" href="password.html">Forgot Password</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError"
                            >Error
                            <div class="sb-sidenav-collapse-arrow">
                                <i class="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="401.html">401 Page</a>
                                <a class="nav-link" href="404.html">404 Page</a>
                                <a class="nav-link" href="500.html">500 Page</a>
                            </nav>
                        </div>
                    </nav>
                </div> -->

                <!-- <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="charts.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Charts</a>
                <a class="nav-link" href="tables.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Tables</a> -->
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Hospital Management System</div>
        </div>
    </nav>
</div>