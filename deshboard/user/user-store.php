<?php
    include('../connect.php');
    session_start();

    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $usertype = $_POST['usertype'];
        $email = $_POST['email'];
        $passwordone = $_POST['passwordone'];
        $passwordtwo = $_POST['passwordtwo'];

        if(empty($name)){
            $_SESSION['msz'] = "Name is required";
            header('location: ./add-user.php');
        }
        elseif(empty($usertype)){
            $_SESSION['msz'] = "Usertype is required";
            header('location: ./add-user.php');
        }
        elseif(empty($email)){
            $_SESSION['msz'] = "Email is required";
            header('location: ./add-user.php');
        }
        elseif(empty($passwordone)){
            $_SESSION['msz'] = "Password is required";
            header('location: ./add-user.php');
        }
        elseif(empty($passwordtwo)){
            $_SESSION['msz'] = "Password is required";
            header('location: ./add-user.php');
        }
        elseif($passwordone !== $passwordtwo){
            $_SESSION['msz'] = "Password not match";
            header('location: ./add-user.php');
        }
        else {    
            $password = password_hash($passwordone, PASSWORD_BCRYPT);

            $sql = "INSERT INTO `user`(`name`, `usertype`, `email`, `password`) 
            VALUES ('$name', '$usertype', '$email', '$password')";

            header('location: ./user-list.php');

            $conn->query($sql);
        }
    }
     else {
       header('location: ./user-list.php');
    }



?>