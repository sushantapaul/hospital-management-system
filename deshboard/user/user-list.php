<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php
    include('../connect.php');

    $sql = "SELECT * FROM `user`";
    $result = $conn->query($sql);

?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>User Account List</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="add-user.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Create Account</a></li>
                            </div>
                        </div>
                        <div style="overflow-x:auto;">
                            <table class="display" id="table_id">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>User Name</th>
                                        <th>User Type</th>
                                        <th>Email Address</th>
                                        <th>Password</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php while($row = $result->fetch_assoc()){ ?>                                
                                <tbody>
                                    <tr>
                                        <td><?php echo $row['id']; ?></td>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['usertype']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['password']; ?></td>
                                        <td>
                                            <a href="" class="view"><i class="fas fa-eye"></i></a>
                                            <a href="" class="edit"><i class="fas fa-edit"></i></a>
                                            <a href="" class="delete"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                              <?php  } ?>
                            </table>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>




