<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>
<?php session_start(); ?>

<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Create User Account</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="user-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Admin User List</a></li>
                            </div>
                        </div>
                        <form action="./user-store.php" method="POST">
                            <div class="alert-danger" role="alert">
                                <?php                                    
                                    if(isset($_SESSION['msz'])){
                                        echo $_SESSION['msz'];
                                        session_destroy();
                                    }                                
                                ?>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-sm-3 col-form-label">User Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="form-control" id="username" placeholder="User Name" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="usertype" class="col-sm-3 col-form-label">User Type</label>
                                <div class="col-sm-6">
                                    <select name="usertype" class="form-control" id="usertype" placeholder="" >
                                        <option value="" disabled selected>Select User Type</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Account Manager">Account Manager</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email Address</label>
                                <div class="col-sm-6">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="passwordone" class="col-sm-3 col-form-label">New Password</label>
                                <div class="col-sm-6">
                                    <input type="password" name="passwordone" class="form-control" id="passwordone" placeholder="New Password" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="passwordtwo" class="col-sm-3 col-form-label">Confirm Password</label>
                                <div class="col-sm-6">
                                    <input type="password" name="passwordtwo" class="form-control" id="passwordtwo" placeholder="Confirm Password" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="activity" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6">
                                <button type="submit" name="submit" class="btn btn-success form-control">save</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>

<?php include('../pertials_deshboard/footer.php')?>




