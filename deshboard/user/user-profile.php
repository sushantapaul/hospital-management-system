<?php include('../pertials_deshboard/header.php')?>
<?php include('../pertials_deshboard/topnav.php')?>


<div id="layoutSidenav">
    <?php include('../pertials_deshboard/sidemenu.php')?>

    <div id="layoutSidenav_content">
        <main>
            <section class="hd-stl">
                <div class="container-fluid">
                    <div class="heading">
                        <h1>Users Profile</h1>
                    </div>
                </div>
            </section>
            <section>
                <div class="container-fluid">
                    <div class="box-area">
                        <div class="pg-btn">
                            <div class="btn-item">
                                <li><a href="user-list.php"><i style="margin-right: 10px" class="fas fa-align-justify"></i>Users List</a></li>
                            </div>
                            <div class="btn-item">
                                <li><a href="add-user.php"><i style="margin-right: 10px" class="fas fa-plus"></i>Add User</a></li>
                            </div>
                        </div>
                        <div class="info-headline mb-4">
                            <h3>User Information</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="id-img">
                                    <img class="img-thumbnail" src="<?php echo $base.'hospital_management_system/dist/assets/img/doctors/doctor-1.png' ?>" alt="">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="id-view">
                                    <table class="table new-table">
                                        <tr>
                                            <td class="left-tbl">User Name</td>
                                            <td class="right-tbl">Dr Akhtarujjaman</td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">User Type</td>
                                            <td class="right-tbl">Doctor</td>
                                        </tr>
                                        <tr>
                                            <td class="left-tbl">Email Address</td>
                                            <td class="right-tbl">abc@gmail.com</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </section>
        </main>

        <!-- <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer> -->
    </div>
</div>


<?php include('../pertials_deshboard/footer.php')?>




