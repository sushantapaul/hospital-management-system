<?php include('./pertials-website/header.php'); ?>
<?php include('./pertials-website/topbar.php'); ?>

<!-- heading dhanmondi start -->
<section style="background: #ebf0f8;">
    <div class="container">
        <!-- <div class="row"> -->
            <div class="headline">
                <h2>Gazipur Branch</h2>
            </div>
        <!-- </div> -->
    </div>
</section>
<section>
    <div class="container">
        <div class="row"><div class="margin-bottom"></div></div>
    </div>
</section>
<section>
    <div class="container">
        <!-- <div class="row"> -->
            <div class="heading">
                <h2>Details</h2>
            </div>
        <!-- </div> -->
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="google-maps">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3652.2307919672403!2d90.38244!3d23.739148!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa52dd6bb11ef4083!2sPopular%20Diagnostic%20Centre%20Ltd%2C%20Dhanmondi%2C%20Dhaka!5e0!3m2!1sen!2sbd!4v1588422309348!5m2!1sen!2sbd" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="solo">
                    <div class="thumbnail">
                        <img src="dist/assets/img/branch/branch1.jpg" alt="branch1">
                        <div class="caption">
                            <h3>Hospital name (Gazipur)</h3>
                            <ul>
                                <li><strong>Address :</strong> House 32/3, sector 2, Gazipur,<br> Dhaka, Bangladesh.</li>
                                <li><strong>Hotline :</strong> 03 945 6584</li>
                                <li><strong>Email :</strong> info.hospitalname.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
            <div class="solo">
                    <div class="thumbnail">
                        <img src="dist/assets/img/branch-md/branch-md-1.jpg" alt="branch-md">
                        <div class="caption">
                            <h3 class="md-name">Dr. M A Kader</h3>
                            <ul>
                                <li class="md-list">Managing Director</li>
                                <li class="md-list">Cardiology Specialist</li>
                                <li class="md-list">Dhanmondi, Dhaka</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- heading dhanmondi end -->
<section>
    <div class="container">
        <div class="row"><div class="margin-bottom"></div></div>
    </div>
</section>



<?php include('./pertials-website/footer.php'); ?>







<!-- aria-hidden="false" tabindex="0" -->