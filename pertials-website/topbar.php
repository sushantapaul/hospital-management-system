<!-- top header start -->
<section>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="top-sl">
                        <marquee behavior="scroll" direction="left" scrollamount="5">Welcome to my Hospital Management System Project.</marquee>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="top-btn">
                        <a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://www.youtube.com"  target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/login"  target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://accounts.google.com/signin/v2/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F%3Ftab%3Drm%26ogbl&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin"  target="_blank"><i class="fa fa-google" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
<!-- top header end -->
<!-- brand header start -->
<section class="big-brand">
    <div class="head-in">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- <div class="row"> -->
                        <div class="brand" data-aos="zoom-in">
                            <a href="index.php"><span>Hospital</span> <span>Management System</span></a>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-3">
                    <div class="info-col">
                        <div class="info-btn">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="info-text">
                            <label>
                                <p class="info-cp m-phn">Hotline <span>(24 hour)</span></p>
                                <p>+880 1922536456</p>
                                <p>+880 1673390188</p>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-col clock">
                        <div class="info-btn">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                        </div>
                        <div class="info-text">
                            <label>
                                <p class="info-cp">Working hour</p>
                                <p>24 hour services</p>
                                <p>Everyday</p>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-col one-of">
                        <div class="info-btn">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="info-text">
                            <label>
                                <p class="info-cp m-phn">Email Us</p>
                                <p><a href="#">boxme.sp@gmail.com</a></p>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mini-brand">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="brand animated bounceInUp slow delay-1s">
                    <a href="index.php"><span>Hospital</span> <span>Management System</span></a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="info-col">
                            <div class="info-btn">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                            <div class="info-text">
                                <label>
                                    <p class="info-cp m-phn">Hotline <span>(24 hour)</span></p>
                                    <p>+880 1922536456</p>
                                    <p>+880 1673390188</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="info-col clock">
                            <div class="info-btn">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </div>
                            <div class="info-text">
                                <label>
                                    <p class="info-cp">Working hour</p>
                                    <p>24 hour services</p>
                                    <p>Everyday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="info-col one-of">
                            <div class="info-btn">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                            <div class="info-text">
                                <label>
                                    <p class="info-cp m-phn">Email Us</p>
                                    <p><a href="#">boxme.sp@gmail.com</a></p>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mic-brand">
    <div class="container">
        <div class="row">
            <div class="hot">
                <p>[Hotline: 02-9254872]</p>
            </div>
        </div>
        <div class="row">
            <div class="brand animated bounceInUp slow delay-1s">
                <a href="index.php"><span>Hospital</span> <span>Management System</span></a>
            </div>
        </div>
    </div>
</section>
<!-- brand header end -->
<!-- navbar start -->
<section>
    <nav class="navbar navbar-default nav nav-justified">
        <div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Home</a></li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Goal & Objectives</a></li>
                    <li><a href="#">Message for Management</a></li>
                    <li><a href="#">Suggesation or Complain</a></li>
                </ul>
                </li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Services <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">incomplete</a></li>
                </ul>
                </li>
                <li><a href="find-doctor.php">Doctors</a></li>
                <li><a href="#">Gallery</a></li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Branches <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="dhanmondi-branch.php">Dhanmondi</a></li>
                    <li><a href="mirpur-branch.php">Mirpus</a></li>
                    <li><a href="savar-branch.php">Savar</a></li>
                    <li><a href="uttara-branch.php">Uttara</a></li>
                    <li><a href="gazipur-branch.php">Gazipur</a></li>
                    <li><a href="mymansing-branch.php">Mymansing</a></li>
                </ul>
                </li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News & Media <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Online Newspaper</a></li>
                    <li><a href="#">Our prospectus</a></li>
                </ul>
                </li>
                <li><a href="#">Notice</a></li>
            </ul>
            </div>
        </div>
        </nav>
</section>
<!-- navbar end -->