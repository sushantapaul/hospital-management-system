<!-- footer section start -->
<section class="end">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="btm-title">
                        <h2>Send any message</h2>
                    </div>
                    <div class="form-area">
                        <form action="./store-message.php" method="POST">
                            <input class="my-form" name="name" type="text" placeholder="Your Legal Name">
                            <input class="my-form" name="number" type="text" placeholder="Contact Number">
                            <input class="my-form" name="message" type="text" placeholder="Your Message">
                            <button class="my-btn" name="submit" type="submit">submit<i class="fa fa-angle-right" aria-hidden="true"></i></button>
                        </form>
                    </div>
                    <!-- <div class="btn-area">
                        <a href="#">
                            <button class="my-btn" name="submit" type="submit">submit<i class="fa fa-angle-right" aria-hidden="true"></i></button>
                        </a>
                    </div> -->
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="btm-title">
                        <h2>Important Links</h2>
                    </div>
                    <div class="extra-link">
                        <p><a href="#">Pharmaceuticals</a></p>
                        <p><a href="#">Medical College</a></p>
                        <p><a href="#">Management Team</a></p>
                        <p><a href="#">Testmonials</a></p>
                        <p><a href="#">Career</a></p>
                        <p><a href="#">Job Circular</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="adr-title">
                        <h2>Contact Us</h2>
                    </div>
                    <div>
                        <table class="footer-tbl">
                            <tr>
                                <td class="tb-name">Address</td>
                                <td class="tb-detail">: House #21, Road # 5, Uttara, Dhaka-1205, Bangladesh</td>
                            </tr>
                            <tr>
                                <td class="tb-name">Phone</td>
                                <td class="tb-detail">: +880 9256487</td>
                            </tr>
                            <tr>
                                <td class="tb-name">E-mail</td>
                                <td class="tb-detail">: info@hospitalmanagement.com</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="top-btn btm-link">
                        <a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://www.youtube.com"  target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/login"  target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://accounts.google.com/signin/v2/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F%3Ftab%3Drm%26ogbl&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin"  target="_blank"><i class="fa fa-google" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="floor">
                <div class="col-md-4">
                    <div class="row">
                        <div class="deck floor-1">
                            <p>Copyright UU &#169; 2020. Hospital Management System</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="deck floor-2">
                            <p>Design & Developed by : &nbsp; <a href="">Sushanta paul</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="deck floor-3">
                            <p><a href="#">Terms and Conditions</a></p>
                            <p><a href="#">Privacy Policy</a></p>
                            <p><a href="#">Customer Support</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- footer section end -->
<button onclick="topFunction()" id="myBtn" title="Go to top">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</button>
    
    
    <script type="text/javaScript" src="dist/assets/lib/jquery/jquery.js"></script>
    <script type="text/javaScript" src="dist/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javaScript" src="dist/assets/lib/aos/aos.js"></script>
    <script type="text/javaScript" src="dist/assets/lib/carousel/js/jquery.min.js"></script>
    <script type="text/javaScript" src="dist/assets/lib/carousel/js/owl.carousel.js"></script>
    
    <script>
    // owl carousel slider start
        $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:4000,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:3,
                nav:true,
                loop:false
            }
        }
    })
    // croll Back To Top Button
    var mybutton = document.getElementById("myBtn");
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    //
    AOS.init({
        once: true,
        duration: 2000,
        easing: 'swing',
    });
    
    </script>
</body>
</html>