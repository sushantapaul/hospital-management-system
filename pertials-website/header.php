<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="John Doe">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>index</title>
    <link type="text/css" rel="stylesheet" href="dist/assets/lib/bootstrap/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="dist/assets/lib/aos/aos.css">
    <link rel="stylesheet" href="dist/assets/lib/carousel/css/owl.carousel.css">
    <link rel="stylesheet" href="dist/assets/lib/carousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/assets/lib/font-awesome/css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="dist/css/webstyle.css">
</head>
<body>