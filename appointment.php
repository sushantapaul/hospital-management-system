<?php include('dist/assets/pertials-website/header.php'); ?>
<?php include('dist/assets/pertials-website/topbar.php');?>

<section style="background: #ebf0f8;">
    <div class="container">
        <div class="row">
            <div class="headline set">
                <h2>Call for Appointment</h2>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row"><div class="margin-bottom"></div></div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="heading set">
                <h2>Hotlines</h2>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="set">
                <div class="col-md-6">
                    <div class="row">
                        <div class="branch br-mg-r">
                            <div class="branch-name">
                                <p>Dhanmondi</p>
                            </div>
                            <div class="branch-number">
                                <p>02-5697158</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="branch br-mg-l">
                            <div class="branch-name">
                                <p>Uttar</p>
                            </div>
                            <div class="branch-number">
                                <p>02-5697159</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="set">
                <div class="col-md-6">
                    <div class="row">
                        <div class="branch br-mg-r">
                            <div class="branch-name">
                                <p>Mirpur</p>
                            </div>
                            <div class="branch-number">
                                <p>02-5697160</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="branch br-mg-l">
                            <div class="branch-name">
                                <p>Gazipur</p>
                            </div>
                            <div class="branch-number">
                                <p>02-5697161</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="set">
                <div class="col-md-6">
                    <div class="row">
                        <div class="branch br-mg-r">
                            <div class="branch-name">
                                <p>Savar</p>
                            </div>
                            <div class="branch-number">
                                <p>02-5697162</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="branch br-mg-l">
                            <div class="branch-name">
                                <p>Mymansing</p>
                            </div>
                            <div class="branch-number">
                                <p>02-5697163</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row"><div class="margin-bottom"></div></div>
    </div>
</section>




<?php include('dist/assets/pertials-website/footer.php');?>
    