<?php include('./pertials-website/header.php'); ?>
<?php include('./pertials-website/topbar.php');?>


<!-- Slide section strat -->
<section>
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="dist/assets/img/slider/slider-1.png" alt="...">
                        <div class="carousel-caption">
                            <h1>Our Doctors are always ready to provide our services</h1>
                            <p>We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="dist/assets/img/slider/slider-2.png" alt="...">
                        <div class="carousel-caption">
                        ...
                        </div>
                    </div>
                    <div class="item">
                        <img src="dist/assets/img/slider/slider-3.png" alt="...">
                        <div class="carousel-caption">
                        ...
                        </div>
                    </div>
                    <div class="item">
                        <img src="dist/assets/img/slider/slider-4.png" alt="...">
                        <div class="carousel-caption">
                        ...
                        </div>
                    </div>
                    <div class="item">
                        <img src="dist/assets/img/slider/slider-5.png" alt="...">
                        <div class="carousel-caption">
                        ...
                        </div>
                    </div>
                    <div class="item">
                        <img src="dist/assets/img/slider/slider-6.png" alt="...">
                        <div class="carousel-caption">
                        ...
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Slide section end -->
<!-- Appoinment section start -->
<section class="unit-a">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="data" data-aos="fade-right">
                    <a href="appointment.php">
                        <i class="fa fa-phone-square" aria-hidden="true"></i>
                        <div class="data-msg">
                            <p>Call for Appointment</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="data" data-aos="fade-up">
                    <a href="find-doctor.php">
                        <i class="fa fa-user-md" aria-hidden="true"></i>
                        <div class="data-msg">
                            <p>Find a Doctor</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="data" data-aos="fade-up">
                    <a href="#">
                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                        <div class="data-msg">
                            <p>Test & Service Charges</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="data">
                    <a href="#">
                        <i class="fa fa-medkit" aria-hidden="true"></i>
                        <div class="data-msg">
                            <p>Blood Bank Section</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Appoinment section end -->
<!-- our services to operations section start -->
<section class="unit-a">
    <div class="container">
        <div class="row">
            <div class="data-sr" data-aos="fade-left">
                <article>
                    <h2>Our Services</h2>
                    <p>We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy. We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy. We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy.</p>
                </article>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="data-df mrg-right" data-aos="zoom-in-right">
                        <div class="df-name">
                            <h2><i class="fa fa-user-md" aria-hidden="true"></i>
                            <span>Our Doctors</span></h2>
                        </div>
                        <p>We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy. We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="data-df  mrg-left" data-aos="zoom-in-left">
                        <div class="df-name">
                            <h2><i class="fa fa-ambulance" aria-hidden="true"></i>
                            <span>Ambulance facilities</span></h2>
                        </div>
                        <p>We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy. We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="data-df mrg-right" data-aos="zoom-in-right">
                        <div class="df-name">
                            <h2><i class="fa fa-h-square" aria-hidden="true"></i>
                            <span>Pathology</span></h2>
                        </div>
                        <p>We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy. We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy.</p>                            
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="data-df  mrg-left" data-aos="zoom-in-left">
                        <div class="df-name">
                            <h2><i class="fa fa-bed" aria-hidden="true"></i>
                            <span>Operations</span></h2>
                        </div>
                        <p>We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy. We are committed to provide affordable services, without any compromise on the quality of service – so that you are able to remain happy.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- our services to operations section end -->
<!-- we care section start  -->
<section>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="data-tc" data-aos="fade-left">
                            <p>WE CARE YOUR HEALTH AND CHECKUP WITH BEST <span>Technology</span></p >
                            <article>Popular Diagnostic Centre Ltd. has a collection of the most advanced medical technologies. All the machineries which are being used here are designed with rigorous safety standards to aid in the diagnosis or treatment of medical problems. Popular Diagnostic Centre Ltd. has a collection of the most advanced medical technologies. All the machineries which are being used here are designed with rigorous safety standards to aid in the diagnosis or treatment of medical problems.</article>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="main-btn" data-aos="fade-right">
                        <a href="#">Know Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</section>
<!-- we care section end  -->
<!-- Why Choose section start -->
<section>
    <div class="container">
        <div class="row">
            <div class="menu-header" data-aos="zoom-in">
                <h1>Why Choose Our Hospital?</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="menu" data-aos="fade-up">
                        <div class="menu-icon">
                            <i class="fa fa-stethoscope" aria-hidden="true"></i>
                        </div>
                        <div class="menu-details">
                            <h1>More experience</h1>
                            <p>Every year, more than a million people come to Mayo Clinic for care. Our highly specialized experts are deeply experienced in treating rare and complex conditions.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3  col-sm-6">
                <div class="row">
                    <div class="menu" data-aos="fade-down">
                        <div class="menu-icon">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <div class="menu-details">
                            <h1>The right answers</h1>
                            <p>Getting effective treatment depends on identifying the right problem. In a recent study, 88 percent of patients who came to Mayo Clinic for a second opinion received a new or refined diagnosis.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3  col-sm-6">
                <div class="row">
                    <div class="menu" data-aos="fade-up">
                        <div class="menu-icon">
                            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                        </div>
                        <div class="menu-details">
                            <h1>Seamless care</h1>
                            <p>At Mayo Clinic, every aspect of your care is coordinated and teams of experts work together to provide exactly the care you need. What might take months elsewhere can often be done in days here.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3  col-sm-6">
                <div class="row">
                    <div class="menu" data-aos="fade-down">
                        <div class="menu-icon">
                            <i class="fa fa-user-md" aria-hidden="true"></i>
                        </div>
                        <div class="menu-details">
                            <h1>Unparalleled expertise</h1>
                            <p>Mayo Clinic experts are some of the best in the world. In the U.S. News & World Report rankings of top hospitals, Mayo Clinic is consistently ranked among the top hospitals in the nation.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="bottom border-btm"></div></div>
    </div>
</section>
<!-- Why Choose section end -->
<!-- Our campus section start -->
<section>
    <div class="container">
        <div class="row">
            <div class="campus-top" data-aos="zoom-in">
                <h1>Our Main Capmus</h1>
            </div>
        </div>
    </div>
</section>
<section class="pos">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="campus" data-aos="zoom-in-left">
                        <img src="dist/assets/img/branch/hospital2.jpg" alt="hospital image">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="text-rt text-bc text-rt-mg" data-aos="zoom-in-right">
                        <p>Major W. J. Virgin, the head of the committee formed to establish the Dhaka Medical College, was the first principal. In the beginning there were only four departments – medicine, surgery, gynecology and otolaryngology (ENT). Since the college did not initially have anatomy or physiology departments, the students at first attended those classes at Mitford Medical School (now the Sir Salimullah Medical College); but, after a month, Professor of Anatomy Pashupati Basuand and Professor of Physiology Hiralal Saha joined the staff and their specialties were taught in ward no. 22 of the hospital.
                        There was no lecture hall nor dissection gallery at first. These needs were met after the construction of new academic buildings in 1955. The college did not have any student housing. Male students were allowed to reside in the Dhaka University's student halls, but female students did not have the use of that facility. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- our campus section end -->
<!-- doctors section start -->
<section class="unit-b">
    <div class="container" id="doctors">
        <div class="row">
            <div class="menu-header" data-aos="zoom-in">
                <h1>Our Doctors</h1>
            </div>
        </div>
    </div>
</section>
<section class="unit-b">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="dr-carousel">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-1.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-2.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-3.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-4.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-5.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-6.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-7.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-8.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-9.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-10.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-11.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="dist/assets/img/doctors/doctor-12.png" alt="">
                                <div class="carousel-text">
                                    <h4>Dr Adum Smith</h4>
                                    <p>Cardiologist</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="text-rt text-bc text-rt-mg" data-aos="fade-left">
                        <p>Major W. J. Virgin, the head of the committee formed to establish the Dhaka Medical College, was the first principal. In the beginning there were only four departments – medicine, surgery, gynecology and otolaryngology (ENT). Since the college did not initially have anatomy or physiology departments, the students at first attended those classes at Mitford Medical School (now the Sir Salimullah Medical College); but, after a month, Professor of Anatomy Pashupati Basuand and Professor of Physiology Hiralal Saha joined the staff and their specialties were taught in ward no. 22 of the hospital. There was no lecture hall nor dissection gallery at first.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="bottom border-btm"></div></div>
    </div>
</section>
<!-- doctors section end -->
<!-- Patient Stories section start -->
<section class="unit-b">
    <div class="container">
        <div class="row">
            <div class="patient-head">
                <h1>Patient Stories</h1>
                <p>At South Eastern Private Hospital, we are committed to making your experience the best experience. We value who you are and how you are, because it's personal.</p>
            </div>
        </div>
    </div>
</section>
<section class="unit-b">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row"><div class="patient-img"></div></div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="text-rt text-bc text-rt-mg">
                        <p>Major W. J. Virgin, the head of the committee formed to establish the Dhaka Medical College, was the first principal. In the beginning there were only four departments – medicine, surgery, gynecology and otolaryngology (ENT). Since the college did not initially have anatomy or physiology departments, the students at first attended those classes at Mitford Medical School (now the Sir Salimullah Medical College); but, after a month, Professor of Anatomy Pashupati Basuand and Professor of Physiology Hiralal Saha joined the staff and their specialties were taught in ward no. 22 of the hospital. There was no lecture hall nor dissection gallery at first.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="last-b"></div></div>
    </div>
</section>
<!-- Patient Stories section end -->
<!-- specialities section start -->
<section>
    <div class="container">
        <div class="row">
            <div class="patient-head pop">
                <h1>Specialties</h1>
                <p>At South Eastern Private Hospital, we are committed to making your experience the best experience. We value who you are and how you are, because it's personal.</p>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail show-pc">
                        <img src="dist/assets/img/patients/patient-care-1.jpg" alt="...">
                        <div class="caption">
                            <a class="caption-link" href="#">
                                <div class="caption-title">
                                    <h1>Relationship</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, during and after treatment for your cancer, supporting you throughout your journey.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail show-pc">
                        <img src="dist/assets/img/patients/patient-care-2.jpg" alt="...">
                        <div class="caption">
                            <a class="caption-link" href="">
                                <div class="caption-title">
                                    <h1>Lab invironment</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, during and after treatment for your cancer, supporting you throughout your journey.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail  show-pc">
                        <img src="dist/assets/img/patients/patient-care-3.jpg" alt="...">
                        <div class="caption">
                            <a class="caption-link" href="">
                                <div class="caption-title">
                                    <h1>Childrens Care</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, during and after treatment for your cancer, supporting you throughout your journey.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail show-pc">
                        <img src="dist/assets/img/patients/patient-care-4.jpg" alt="...">
                        <div class="caption">
                            <a class="caption-link" href="">
                                <div class="caption-title">
                                    <h1>Careness</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="bottom border-btm show-pc"></div></div>
    </div>
</section>
<section>
    <!-- show only max-width 768px -->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail tab">
                        <div class="caption">
                            <a class="caption-link" href="#">
                                <div class="caption-title">
                                    <h1>Relationship</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, during and after treatment for your cancer, supporting you throughout your journey.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail tab">
                        <div class="caption">
                            <a class="caption-link" href="">
                                <div class="caption-title">
                                    <h1>Lab invironment</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, during and after treatment for your cancer, supporting you throughout your journey.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail tab">
                        <div class="caption">
                            <a class="caption-link" href="">
                                <div class="caption-title">
                                    <h1>Childrens Care</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, during and after treatment for your cancer, supporting you throughout your journey.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="row">
                    <div class="thumbnail tab">
                        <div class="caption">
                            <a class="caption-link" href="">
                                <div class="caption-title">
                                    <h1>Careness</h1>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </a>
                            <p>Our oncology program is designed to help you manage the physical and emotional impacts before, </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="bottom border-btm tab"></div></div>
    </div>
</section>
<!-- specialities section end -->
<!-- Our Technology start -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="gl-head">
                        <a href="#">Our Latest <span>Technology</span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="gl-text">
                        <p>At Popular Diagnostic Centre Ltd. We have been actively revamping our technologies with the most modern one since the first day of our journey. All the aged machineries are being regularly replaced with the latest one. Technical team members are maintaining the machineries actively so that these can provide the best outcome.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-1.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Gray and black electronic devices</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-8.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-10.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-3.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-4.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-5.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-6.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="row">
                    <div class="tools-gl">
                        <a href="#" class="thumbnail">
                            <img src="dist/assets/img/technology/technology-15.jpg" alt="...">
                        </a>
                        <div class="overlay">
                            <p class="text">Image title</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Our Technology end -->
<!-- manageing director section start -->
<section class="md-mini">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="text-rt text-bc md-mg">
                        <h2>managing director</h2>
                        <h3>Dr. m. d. kabir</h3>
                        <p> Major W. J. Virgin, the head of the committee formed to establish the Dhaka Medical College, was the first principal. In the beginning there were only four departments – medicine, surgery, gynecology and otolaryngology (ENT). Since the college did not initially have anatomy or physiology departments, the students at first attended those classes at Mitford Medical School (now the Sir Salimullah Medical College); but, after a month, Professor of Anatomy Pashupati Basuand and Professor of Physiology Hiralal Saha joined the staff and their specialties were taught in ward no. 22 of the hospital. There was no lecture hall nor dissection gallery at first.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="md-img"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- manageing director section end -->
<!-- for online section start -->
<section>
    <div class="demo">
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="col-md-8">
                        <div class="card-text">
                            <p>Emergency <span>health tips</span> or <span>ambulance</span> service <span>24 hour</span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-call">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> 02-9241587</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- for online section end -->
<!-- video section start -->
<section class="videos">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="dist/assets/videos/Apollo-Hospital-Dhaka.mp4" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="text-rt text-rt-mg video-mg v-tab">
                        <h2>A simple video in our Hospital</h2>
                        <p> Major W. J. Virgin, the head of the committee formed to establish the Dhaka Medical College, was the first principal. In the beginning there were only four departments – medicine, surgery, gynecology and otolaryngology (ENT). Since the college did not initially have anatomy or physiology departments, the students at first attended those classes at Mitford Medical School (now the Sir Salimullah Medical College); but, after a month, Professor of Anatomy Pashupati Basuand and Professor of Physiology Hiralal Saha joined the staff and their specialties were taught in ward no. 22 of the hospital. There was no lecture hall nor dissection gallery at first.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="bottom border-btm show-pc"></div></div>
    </div>
</section>
<!-- video section end -->


<?php include('./pertials-website/footer.php');?>
    