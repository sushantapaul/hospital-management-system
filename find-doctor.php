<?php include('./pertials-website/header.php'); ?>
<?php include('./pertials-website/topbar.php');?>

<section style="background: #ebf0f8;">
    <div class="container">
        <div class="row">
            <div class="headline set">
                <h2>Find Doctors</h2>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row"><div class="margin-bottom"></div></div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="heading set">
                <h2>Search</h2>
            </div>
        </div>
    </div>
</section>
<!-- find doctor start -->
<section>
    <div class="container">
        <div class="row">
            <div class="set">
                <form class="form-horizontal">
                    <div class="form-group has-success has-feedback">
                        <label class="control-label col-sm-3" for="inputSuccess3">Select Branch</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="branch-name" id="inputSuccess3" aria-describedby="inputSuccess3Status">
                                <option disabled selected>Select a branch</option>
                                <option>Dhanmondi</option>
                                <option>Uttara</option>
                                <option>Gazipur</option>
                                <option>Rajshahi</option>
                                <option>Savar</option>
                                <option>Mymansing</option>
                            </select>
                            <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                            <span id="inputSuccess3Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <label class="control-label col-sm-3" for="inputSuccess3">Search by Speciality</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="Speciality" id="inputSuccess3" aria-describedby="inputSuccess3Status">
                                <option disabled selected>Select by Speciality</option>
                                <option>Cardiology</option>
                                <option>Neurology</option>
                                <option>Gynaecology</option>
                                <option>Medicine</option>
                                <option>Child Neurology</option>
                                <option>Skin / Dermatology</option>
                                <option>Eye / Optharmology</option>
                            </select>
                            <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                            <span id="inputSuccess3Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <label class="control-label col-sm-3" for="inputSuccess3">Search by Days</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="day" id="inputSuccess3" aria-describedby="inputSuccess3Status">
                                <option disabled selected>Select day</option>
                                <option>Friday</option>
                                <option>Saturday</option>
                                <option>Sunday</option>
                                <option>Monday</option>
                                <option>Tuesday</option>
                                <option>Wednesday</option>
                                <option>Thursday</option>
                            </select>
                            <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                            <span id="inputSuccess3Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <div class="col-sm-3" for="inputSuccess3"></div>
                        <div class="col-sm-9">
                            <button type="button" name="submit" class="btn btn-success btn-block">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- find doctor end -->
<section>
    <div class="container">
        <div class="row"><div class="margin-bottom"></div></div>
    </div>
</section>



<?php include('./pertials-website/footer.php');?>